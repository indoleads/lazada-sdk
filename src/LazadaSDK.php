<?php
/*
 * Project:         lazada-sdk
 * File:            LazadaSDK.php
 * Date:            2021-04-23
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

namespace LazadaSDK;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use LazadaSDK\Exceptions\IspException;
use LazadaSDK\Exceptions\IsvException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use LazadaSDK\Exceptions\SystemException;

class LazadaSDK
{
    /** @var string */
    private $appKey;

    /** @var string */
    private $appSecret;

    /** @var string */
    private $accessToken;

    /** @var string */
    private $userToken;

    /** @var string */
    private $apiGateway;

    /** @var int */
    private $connectionTimeout;

    /** @var float */
    private $readTimeout;

    /** @var Request */
    public $request;

    /** @var string */
    private $signMethod = 'sha256';

    /**
     * Lazada API auth URI
     *
     * @var string
     */
    private $authURL = 'https://auth.lazada.com/rest';

    /**
     * Lazada API URLs
     *
     * @var string[]
     */
    private $apiGatewayURLs = [
        'id' => 'https://api.lazada.co.id/rest/',
        'my' => 'https://api.lazada.com.my/rest/',
        'ph' => 'https://api.lazada.com.ph/rest/',
        'sg' => 'https://api.lazada.sg/rest/',
        'th' => 'https://api.lazada.co.th/rest/',
        'vn' => 'https://api.lazada.vn/rest/',
    ];

    /**
     * Debug mode
     * @var bool
     */
    private $debug = false;

    /**
     * Guzzle debug mode
     * @var bool
     */
    private $debugGuzzle = false;

    /**
     * LazadaSDK constructor.
     *
     * Format for $config if provided:
     * $config = [
     *     'apiGateway'     => 'ph',
     *     'appKey'         => 'xxxxx',
     *     'appSecret'      => 'xxxxxxxxx',
     *     'accessToken'    => 'xxxxxxxxx',
     * ];
     *
     * @param array|null $config
     * @throws RuntimeException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    private function __construct(?array $config)
    {
        if (!empty($config)) {
            if (isset($config['apiGateway'])) {
                if (!array_key_exists($config['apiGateway'], $this->apiGatewayURLs)) {
                    throw new RuntimeException("There is no API URL defined for the country code \"{$config['apiGateway']}\"!");
                }
                $this->apiGateway = $this->apiGatewayURLs[$config['apiGateway']];
            }

            if (isset($config['appKey'])) {
                $this->appKey = $config['appKey'];
            }

            if (isset($config['appSecret'])) {
                $this->appSecret = $config['appSecret'];
            }

            if (isset($config['userToken'])) {
                $this->userToken = $config['userToken'];
            }

            if (isset($config['debug'])) {
                $this->debug = $config['debug'];
            }

            if (isset($config['debugGuzzle'])) {
                $this->debugGuzzle = $config['debugGuzzle'];
            }
        }
    }

    /**
     * Create an instance of this SDK
     *
     * @param array|null $config
     *
     * @return LazadaSDK
     * @throws RuntimeException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public static function init(?array $config): LazadaSDK
    {
        return new self($config);
    }

    /**
     * Returns the appKey
     *
     * @return string|null
     */
    public function getAppKey(): ?string
    {
        return $this->appKey;
    }

    /**
     * Set the appKey
     *
     * @param string $appKey
     */
    public function setAppKey(string $appKey): void
    {
        $this->appKey = $appKey;
    }

    /**
     * Returns the Secret Key
     *
     * @return string|null
     */
    public function getAppSecret(): ?string
    {
        return $this->appSecret;
    }

    /**
     * Set the secret key
     *
     * @param string $appSecret
     */
    public function setAppSecret(string $appSecret): void
    {
        $this->appSecret = $appSecret;
    }

    /**
     * Returns the API URL
     *
     * @return string|null
     */
    public function getApiGateway(): ?string
    {
        return $this->apiGateway;
    }

    /**
     * Set the API gateway URL based on the given URL or country ISO code (2 letter!)
     *
     * @param string $apiGateway The API gateway URL or the country ISO code (2 letter!)
     */
    public function setApiGateway(string $apiGateway): void
    {
        if (strlen($apiGateway) === 2) {
            if (!array_key_exists($apiGateway, $this->apiGatewayURLs)) {
                throw new RuntimeException("There is no API URL defined for the country code \"{$apiGateway}\"!");
            }
            $this->apiGateway = $this->apiGatewayURLs[$apiGateway];
        } else {
            if (Utils::endsWith($apiGateway, '/')) {
                $this->apiGateway = substr($apiGateway, 0, -1);
            } else {
                $this->apiGateway = $apiGateway;
            }
        }
    }

    /**
     * Returns the connection timeout
     *
     * @return int
     */
    public function getConnectionTimeout(): int
    {
        return $this->connectionTimeout ?? 0;
    }

    /**
     * Set the connection timeout
     *
     * @param int $connectionTimeout
     */
    public function setConnectionTimeout(int $connectionTimeout): void
    {
        $this->connectionTimeout = $connectionTimeout;
    }

    /**
     * Returns the read timeout
     *
     * default: 15.0
     *
     * @return float
     */
    public function getReadTimeout(): float
    {
        return $this->readTimeout ?? 15.0;
    }

    /**
     * Set the read timeout
     *
     * @param float $readTimeout
     */
    public function setReadTimeout(float $readTimeout): void
    {
        $this->readTimeout = $readTimeout;
    }

    /**
     * Returns the signature method
     *
     * @return string|null
     */
    public function getSignMethod(): ?string
    {
        return $this->signMethod;
    }

    /**
     * Set the signature method
     *
     * @param string $signMethod
     */
    public function setSignMethod(string $signMethod): void
    {
        $this->signMethod = $signMethod;
    }

    /**
     * Returns the access token
     *
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    /**
     * Set the access token
     *
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Returns the user token
     *
     * @return string|null
     */
    public function getUserToken(): ?string
    {
        return $this->userToken;
    }

    /**
     * Set the user token
     *
     * @param string $userToken
     */
    public function setUserToken(string $userToken): void
    {
        $this->userToken = $userToken;
    }

    /**
     * Returns the status of the debug mode
     *
     * @return bool
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function debugMode(): bool
    {
        return $this->debug;
    }

    /**
     * Returns the status of the debug mode for Guzzle
     *
     * @return bool
     * @author Steffen Haase <s.haase@indoleads.com>
     */
    public function guzzleDebugMode(): bool
    {
        return $this->debugGuzzle;
    }

    /**
     * Initialize an request
     *
     * @param string $endpoint
     * @param string $method
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function request(string $endpoint, string $method = 'GET'): void
    {
        $this->request = new Request($this, $endpoint, $method);
    }

    public function response(): ResponseInterface
    {
        return $this->request->getResponse();
    }

    /**
     * Get all offers
     *
     * @param array $query
     *
     * @return array
     * @throws Exception|IspException|IsvException|SystemException|GuzzleException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getOffers(array $query): array
    {
        $this->request('/marketing/offer/list/get');
        $this->request->addQueryParameters($query);
        $this->request->execute();

        $res = $this->request->getConvertedResponse();
        Utils::checkForErrorResponse($res);

        if (isset($res['result']['data'])) {
            return $res['result']['data'];
        }

        return [];
    }

    /**
     * Get all bonus offers
     *
     * @param array $query
     *
     * @return array
     * @throws Exception|IspException|IsvException|SystemException|GuzzleException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getBonusOffers(array $query): array
    {
        $this->request('/marketing/bonus/offer/list/get');
        $this->request->addQueryParameters($query);
        $this->request->execute();

        $res = $this->request->getConvertedResponse();
        Utils::checkForErrorResponse($res);

        if (isset($res['result']['data'])) {
            return $res['result']['data'];
        }

        return [];
    }

    /**
     * Get conversions
     *
     * @param array $query
     *
     * @return array
     * @throws Exception|IspException|IsvException|SystemException|GuzzleException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getConversions(array $query): array
    {
        $this->request('/marketing/conversion/report');
        $this->request->addQueryParameters($query);
        $this->request->execute();

        $res = $this->request->getConvertedResponse();
        Utils::checkForErrorResponse($res);

        if (isset($res['result']['data'])) {
            return $res['result']['data'];
        }

        return [];
    }

    /**
     * Get the promo link for a specific offer
     *
     * @param string $offerId
     *
     * @return array
     * @throws Exception|GuzzleException|IspException|IsvException|SystemException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getPromoLink(string $offerId): array
    {
        $this->request('/marketing/offers/link/get');
        $this->request->addQueryParameter('offerId', $offerId);
        $this->request->execute();

        $res = $this->request->getConvertedResponse();
        Utils::checkForErrorResponse($res);

        if (isset($res['result']['data'])) {
            return $res['result']['data'];
        }

        return [];
    }

    /**
     * Get the performance report
     *
     * @param array $query
     *
     * @return array
     * @throws Exception|GuzzleException|IspException|IsvException|SystemException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getPerformanceReport(array $query): array
    {
        $this->request('/marketing/performance/get');
        $this->request->addQueryParameters($query);
        $this->request->execute();

        $res = $this->request->getConvertedResponse();
        Utils::checkForErrorResponse($res);

        if (isset($res['result']['data'])) {
            return $res['result']['data'];
        }

        return [];
    }
}
