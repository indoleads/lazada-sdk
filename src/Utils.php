<?php
/*
 * Project:         lazada-sdk
 * File:            Utils.php
 * Date:            2021-04-23
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

namespace LazadaSDK;

use Exception;
use LazadaSDK\Exceptions\IspException;
use LazadaSDK\Exceptions\IsvException;
use LazadaSDK\Exceptions\SystemException;
use stdClass;

class Utils
{
    /**
     * Check if string $haystack starts with $needle
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     * @author Steffen Haase <s.haase@indoleads.com>
     */
    public static function startsWith(string $haystack, string $needle): bool
    {
        $len = mb_strlen($needle);
        return (mb_substr($haystack, 0, $len) === $needle);
    }

    /**
     * Check if string (multibyte) $haystack ends with $needle
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     * @author Steffen Haase <s.haase@indoleads.com>
     */
    public static function endsWith(string $haystack, string $needle): bool
    {
        $len = mb_strlen($needle);
        if ($len == 0) return true;
        return (mb_substr($haystack, -$len) === $needle);
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @param string $appSecret
     * @param string $algo
     *
     * @return string
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public static function generateSignature(string $endpoint, array $params, string $appSecret, string $algo): string
    {
        ksort($params);
        $stringToBeSigned = $endpoint;

        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }

        return strtoupper(hash_hmac($algo, $stringToBeSigned, $appSecret));
    }

    /**
     * Returns the current time
     *
     * @return string
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public static function getMsecTime(): string
    {
        $seconds = explode(' ', microtime())[1];
        return $seconds . '000';
    }

    /**
     * Check if the response is a error response (ISP|ISV|SYSTEM)
     * @param array|stdClass $response
     *
     * @throws Exception|IsvException|IspException|SystemException
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public static function checkForErrorResponse($response): void
    {
        if (!is_array($response) && !is_object($response)) {
            throw new Exception(
                'Invalid response format for check! The response must be passed as JSON object ' .
                'or associative array!' . PHP_EOL . var_export($response, true)
            );
        }

        if (is_array($response)) {
            if ($response['code'] !== '0') {
                if ($response['type'] === 'ISP') {
                    throw new IspException($response['message'] . " (Code: {$response['code']})");
                }

                if ($response['type'] === 'ISV') {
                    throw new IsvException($response['message'] . " (Code: {$response['code']})");
                }

                if ($response['type'] === 'SYSTEM') {
                    throw new SystemException($response['message'] . " (Code: {$response['code']})");
                }
            }
        }

        if (is_object($response)) {
            if ($response->code !== '0') {
                if ($response->type === 'ISP') {
                    throw new IspException($response->message . " (Code: $response->code)");
                }

                if ($response->type === 'ISV') {
                    throw new IsvException($response->message . " (Code: $response->code)");
                }

                if ($response->type === 'SYSTEM') {
                    throw new SystemException($response->message . " (Code: $response->code)");
                }
            }
        }
    }
}
