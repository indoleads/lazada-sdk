<?php
/*
 * Project:         lazada-sdk
 * File:            SystemException.php
 * Date:            2021-04-26
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

namespace LazadaSDK\Exceptions;

use Exception;

class SystemException extends Exception
{

}
