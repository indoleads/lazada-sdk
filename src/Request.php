<?php
/*
 * Project:         lazada-sdk
 * File:            Request.php
 * Date:            2021-04-23
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

namespace LazadaSDK;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use stdClass;

class Request
{
    /** @var string */
    public $endpoint;

    /** @var array */
    public $headers = [];

    /** @var array */
    public $queryParameter = [];

    /** @var array */
    public $postParameter = [];

    /** @var string */
    public $method = 'GET';

    /** @var ResponseInterface */
    private $response;

    private $responseHeaders;
    private $convertedResponse;

    /** @var LazadaSDK */
    private $sdk;

    /**
     * Request constructor.
     *
     * @param LazadaSDK   $sdk
     * @param string      $endpoint
     * @param string|null $method
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function __construct(LazadaSDK $sdk, string $endpoint, string $method)
    {
        if (strpos($endpoint, '//') === 0) {
            $endpoint = substr($endpoint, 1);
        }

        $this->sdk = $sdk;
        $this->endpoint = $endpoint;
        $this->method = strtoupper($method);
    }

    /**
     * Add a new query parameter with related value
     *
     * @param string $key
     * @param        $value
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function addQueryParameter(string $key, $value): void
    {
        $this->queryParameter[$key] = $value;
    }

    /**
     * Add multiple query parameter
     *
     * @param array $data
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function addQueryParameters(array $data): void
    {
        foreach ($data as $k => $v) {
            $this->queryParameter[$k] = $v;
        }
    }

    /**
     * Add a new POST parameter with related value
     *
     * @param string $key
     * @param        $value
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function addPostParameter(string $key, $value): void
    {
        $this->postParameter[$key] = $value;
    }

    /**
     * Add a new header
     *
     * @param string $key
     * @param string $value
     *
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function addHeader(string $key, string $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * Executes the request
     *
     * @throws GuzzleException
     */
    public function execute()
    {
        if (empty($this->sdk->getApiGateway())) {
            throw new RuntimeException(
                'There is no API gateway specified! Please pass the country ISO code when calling the ' .
                '"init()" method or pass the gateway URL/country ISO code via "setApiURL()".'
            );
        }
        $sysParameter = [
            'app_key'       => $this->sdk->getAppKey(),
            'sign_method'   => $this->sdk->getSignMethod(),
            'userToken'     => $this->sdk->getUserToken(),
            'timestamp'     => Utils::getMsecTime(),
        ];

        $client = new Client([
            'base_uri' => $this->sdk->getApiGateway(),
            'timeout' => $this->sdk->getReadTimeout()
        ]);

        if ($this->method === 'GET') {
            $query = array_merge($this->queryParameter, $sysParameter);
            $query['sign'] = Utils::generateSignature(
                $this->endpoint,
                $query,
                $this->sdk->getAppSecret(),
                $this->sdk->getSignMethod()
            );

            $options = [
                'query' => $query,
                'debug' => $this->sdk->debugMode() && $this->sdk->guzzleDebugMode(),
            ];

            /**
             * Need to remove the leading "/" from the endpoint (if exists) to prevent wrong
             * "base uri" + "relative uri" combination by guzzle.
             *
             * @see https://docs.guzzlephp.org/en/stable/quickstart.html
             */
            $endpoint = $this->endpoint;
            if (Utils::startsWith($endpoint, '/')) {
                $endpoint = substr($endpoint, 1);
            }

            $requestURI = $this->sdk->getApiGateway() . $endpoint . '?' . http_build_query($query);
            if ($this->sdk->debugMode()) {
                echo "REQUEST URI: $requestURI\n\n";
            }

            $this->response = $client->request($this->method, $endpoint, $options);
            $this->convertedResponse = $this->convertResponse();

            $resHeaders = $this->response->getHeaders();
            $headers = '';
            foreach ($resHeaders as $name => $v) {
                $headers .= $name . ': ' . $v[0] . PHP_EOL;
            }
            $this->responseHeaders = $headers;
        }
    }

    /**
     * Returns the response
     *
     * @return ResponseInterface
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * Returns the body content of the response
     *
     * @return mixed
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    public function getResponseBody()
    {
        return $this->response->getBody()->getContents();
    }

    public function getConvertedResponse()
    {
        return $this->convertedResponse;
    }

    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * This method decode the JSON response and return it as object or array
     *
     * Returns NULL in case of an complete empty response.
     *
     * @param bool $assoc
     * @param int  $depth
     * @param int  $flags
     *
     * @return array|stdClass|null
     * @throws RuntimeException
     * @see    https://www.php.net/manual/en/function.json-decode.php
     * @see    https://www.php.net/manual/en/function.json-last-error.php
     * @author Steffen Haase <s.haase@indoleds.com>
     */
    private function convertResponse(bool $assoc = true, int $depth = 512, int $flags = 0)
    {
        $data = $this->response->getBody()->getContents();

        if (!empty($data)) {
            $decoded = json_decode($data, $assoc, $depth, $flags);

            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    $error = 'Maximum stack depth exceeded!';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $error = 'Underflow or the modes mismatch!';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $error = 'Unexpected control character found!';
                    break;
                case JSON_ERROR_SYNTAX:
                    $error = 'Syntax error, malformed JSON!';
                    break;
                case JSON_ERROR_UTF8:
                    $error = 'Malformed UTF-8 characters, possibly incorrectly encoded!';
                    break;
                case JSON_ERROR_UTF16:
                    $error = 'Malformed UTF-16 characters, possibly incorrectly encoded!';
                    break;
                default:
                    $error = '';
                    break;
            }

            if ($error !== '') {
                throw new RuntimeException($error . PHP_EOL . 'Response: ' . var_export($data, true));
            }

            return $decoded;
        }

        return null;
    }
}
