# Lazada SDK

This SDK provides methods to interact with Lazada's OpenAPI.

## Repository Migration to Bitbucket
To update your local Git repository you need to set the remote origin to the new repository location:

Bash:
```bash
user@machine:~/path/to/local/repository$ git remote set-url origin git@bitbucket.org:indoleads/lazada-sdk.git
user@machine:~/path/to/local/repository$ git remote -v
origin	git@bitbucket.org:indoleads/lazada-sdk.git (fetch)
origin	git@bitbucket.org:indoleads/lazada-sdk.git (push)
```

## Prerequisites

- PHP >= 7.1
- Composer
- ext-json
- ext-mbstring

## Dependencies

- Guzzle ~6.0

## Installation

Execute the command below to install this package

```bash
composer require indoleads/lazada-sdk
```

## Usage

There are 2 ways to operate with this SDK:
1. Using pre-defined methods
2. Call API endpoints explicit

### 1. Using pre-defined methods
```php
use LazadaSDK\LazadaSDK;

$config = [
    'apiGateway'    => '',  // Country ISO code (2 letter, e.g. ph|id|my|vn|sg|th) or URL
    'appKey'        => '',  // App key
    'appSecret'     => '',  // App secret
    'userToken'     => ''   // User Token
];
    
$sdk = LazadaSDK::init($config);

/* Get all offers/bonus offers */
$queryParams = [
    'limit'     => 1000,
    'page'      => 1,
];

$offers = $sdk->getOffers($queryParams); // Returns an associative array
$bonusOffers = $sdk->getBonusOffers($queryParams); // Returns an associative array

/**
 * Example: Get conversions
 * 
 * 1. We need the offer IDs from our requests above
 * 2. We need a single date or an date range (in this example we use a date range)
 * 3. Get for each day/offer ID the conversions
 */
 
// Step #1:
$offerIds = [];
if (!empty($offers)) {
    foreach ($offers as $offer) {
        if (strtolower($offer['type']) === 'cps') {
            $offerIds[] = $offer['offerId'];
        }
    }
}
if (!empty($bonusOffers)) {
    foreach ($offers as $offer) {
        if (strtolower($offer['type']) === 'cps') {
            $offerIds[] = $offer['offerId'];
        }
    }
}

// Step #2:
$period = new DatePeriod(
     new DateTime('2010-10-01'),
     new DateInterval('P1D'),
     new DateTime('2010-10-05')
);

// Step #3:
foreach ($period as $date) {
    foreach ($offerIds as $key => $offerId) {
        $queryParams = [
            'userToken' => $sdk->getUserToken(),
            'dateStart' => $date->format('Y-m-d'),
            'dateEnd' => $date->format('Y-m-d'),
            'offerId' => $offerId,
            'limit' => 100,
            'page' => 1,
        ];
    
        $conversions = $sdk->getConversions($queryParams); // Returns an associatives array
    }
}
```

### 2. Call API endpoints explicit
The example code below returns the same as the when we would call the method `$sdk->getOffers($queryParams);` like in the code above.
```php
use LazadaSDK\LazadaSDK;

$config = [
    'apiGateway'    => '',  // Country ISO code (2 letter, e.g. ph|id|my|vn|sg|th) or URL
    'appKey'        => '',  // App key
    'appSecret'     => '',  // App secret
    'userToken'     => ''   // User Token
];
    
$sdk = LazadaSDK::init($config);

/* Get all offers */
$queryParams = [
    'limit'     => 1000,
    'page'      => 1,
];

$sdk->request('/marketing/offer/list/get');
$sdk->request->addQueryParameters($queryParams);
$sdk->request->execute();

$response = $sdk->response();
$responseBody = $response->getBody()->getContents();

$offers = json_decode($responseBody);
```

## License

This SDK is licensed under the MIT license.
