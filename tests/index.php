<?php
/*
 * Project:         lazada-sdk
 * File:            index.php
 * Date:            2021-04-23
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

require_once '../vendor/autoload.php';

use GuzzleHttp\Exception\GuzzleException;
use LazadaSDK\LazadaSDK;
use LazadaSDK\Utils;

$config = [
    'apiGateway'    => '',
    'appKey'        => '',
    'appSecret'     => '',
    'userToken'     => '',
    'debug'         => false,
];
$offerIds = [];
$offers = null;

$queryParams = [
    'limit'     => 1000,
    'page'      => 1,
];

$sdk = LazadaSDK::init($config);

$offers = $sdk->getOffers($queryParams);
var_dump($offers);
if (!empty($offers)) {
    foreach ($offers as $offer) {
        if (strtolower($offer['type']) === 'cps') {
            $offerIds[] = $offer['offerId'];
        }
    }
}

$offers = $sdk->getBonusOffers($queryParams);
if (!empty($offers)) {
    foreach ($offers as $offer) {
        if (strtolower($offer['type']) === 'cps') {
            $offerIds[] = $offer['offerId'];
        }
    }
}

echo "OFFER IDs: " . implode(', ', $offerIds) . PHP_EOL;

$dates = Utils::dateRangeObjects('2021-04-01', '2021-04-26');
foreach ($dates as $date) {
    foreach ($offerIds as $key => $offerId) {
        $page = 1;

        $queryParams = [
            'userToken' => $sdk->getUserToken(),
            'dateStart' => $date->format('Y-m-d'),
            'dateEnd' => $date->format('Y-m-d'),
            'offerId' => $offerId,
            'limit' => 100,
            'page' => $page,
        ];

        try {
            $conversions = $sdk->getConversions($queryParams);
            echo "RESPONSE:\n" . var_export($conversions, true) . PHP_EOL;
        } catch (Exception|GuzzleException $e) {
            echo $e->getMessage() . PHP_EOL . $e->getTraceAsString();
        }

    }
}
