<?php
/*
 * Project:         lazada-sdk
 * File:            LazadaSdkTest.php
 * Date:            2021-04-23
 * Author:          Steffen Haase <s.haase@indoleads.com>
 * Copyright:       (c) 2021 Indoleads.com/Oneklix.com
 */

use GuzzleHttp\Exception\GuzzleException;
use LazadaSDK\LazadaSDK;
use PHPUnit\Framework\TestCase;

final class LazadaSdkTest extends TestCase
{
    private $config = [
        'apiGateway'    => '',  // Country ISO code (2 letter)
        'appKey'        => '',  // App key
        'appSecret'     => '',  // App secret
        'userToken'     => ''   // User Token
    ];

    public function testCanInitializeTheSdk(): void
    {
        $this->assertInstanceOf(
            LazadaSDK::class,
            LazadaSDK::init($this->config)
        );
    }

    public function testGetOffers(): void
    {
        $sdk = LazadaSDK::init($this->config);

        $queryParams = [
            'limit'     => 1000,
            'page'      => 1,
        ];

        $offers = $sdk->getOffers($queryParams);

        $response = $sdk->response();
        $this->assertEquals(200, $response->getStatusCode());

        if (!empty($offers)) {
            $resp = $sdk->request->getResponseBody();

            if (!empty($resp)) {
                $this->assertIsArray($resp);
                $this->assertArrayHasKey('result', $resp);
                $this->assertArrayHasKey('data', $resp['result']);
                $this->assertIsArray($resp['result']['data']);
            }

        }
    }

    public function testGetBonusOffers(): void
    {
        $sdk = LazadaSDK::init($this->config);

        $queryParams = [
            'limit'     => 1000,
            'page'      => 1,
        ];

        $offers = $sdk->getBonusOffers($queryParams);

        $response = $sdk->response();
        $this->assertEquals(200, $response->getStatusCode());

        if (!empty($offers)) {
            $resp = $sdk->request->getResponseBody();

            if (!empty($resp)) {
                $this->assertArrayHasKey('result', $resp);
                $this->assertArrayHasKey('data', $resp['result']);
                $this->assertIsArray($resp['result']['data']);
            }
        }
    }

    public function testGetConversions(): void
    {
        $sdk = LazadaSDK::init($this->config);

        $queryParams = [
            'limit'     => 1000,
            'page'      => 1,
        ];

        $offers = $sdk->getOffers($queryParams);

        $offerIds = [];
        if (!empty($offers)) {
            foreach ($offers as $offer) {
                if (strtolower($offer['type']) === 'cps') {
                    $offerIds[] = $offer['offerId'];
                }
            }
        } else {
            echo "No offer IDs to get conversions :(." . PHP_EOL;
            return;
        }

        $random = random_int(0, (count($offerIds) - 1));

        $queryParams = [
            'dateStart' => '2021-04-01',
            'dateEnd' => '2021-04-26',
            'offerId' => $offerIds[$random],
            'limit' => 100,
            'page' => 1,
        ];

        try {
            $sdk->getConversions($queryParams);
        } catch (Exception|GuzzleException $e) {
            echo $e->getMessage() . PHP_EOL . $e->getTraceAsString();
        }

        $response = $sdk->response();
        $this->assertEquals(200, $response->getStatusCode());

        $resp = $sdk->request->getResponseBody();

        if (!empty($resp)) {
            $this->assertIsArray($resp);
            $this->assertArrayHasKey('result', $resp);
            $this->assertArrayHasKey('data', $resp['result']);
            $this->assertIsArray($resp['result']['data']);
        }
    }

    public function testGetPromoLink(): void
    {
        $sdk = LazadaSDK::init($this->config);

        $queryParams = [
            'limit'     => 1000,
            'page'      => 1,
        ];

        $offers = $sdk->getOffers($queryParams);

        $offerIds = [];
        if (!empty($offers)) {
            foreach ($offers as $offer) {
                if (strtolower($offer['type']) === 'cps') {
                    $offerIds[] = $offer['offerId'];
                }
            }
        } else {
            echo "No offer IDs to get conversions :(." . PHP_EOL;
            return;
        }

        $random = random_int(0, (count($offerIds) - 1));

        $sdk->getPromoLink($offerIds[$random]);

        $response = $sdk->response();
        $this->assertEquals(200, $response->getStatusCode());

        $resp = $sdk->request->getResponseBody();

        if (!empty($resp)) {
            $this->assertArrayHasKey('result', $resp);
            $this->assertArrayHasKey('data', $resp['result']);
            $this->assertIsArray($resp['result']['data']);
        }
    }

    public function testGetPerformanceReport(): void
    {
        $sdk = LazadaSDK::init($this->config);

        $query = [
            'dateStart' => '2021-04-01',
            'dateEnd'   => '2021-04-26',
            'offerType' => 'cps',
            'limit'     => 1000,
            'page'      => 1,
        ];
        $report = $sdk->getPerformanceReport($query);

        var_dump($report);

        $response = $sdk->response();
        $this->assertEquals(200, $response->getStatusCode());

        if (!empty($offers)) {
            $resp = $sdk->request->getResponseBody();

            if (!empty($resp)) {
                $this->assertArrayHasKey('result', $resp);
                $this->assertArrayHasKey('data', $resp['result']);
                $this->assertIsArray($resp['result']['data']);
            }
        }
    }
}
